### ADB 使用介绍
##### ADB -> Android Debug Bridge
#### 常用命令
1.查看当前连接的设备 `adb devices`  
2.重启设备 `adb reboot`  
3.安装apk `adb install -r {apkFilePath}`  
4.卸载apk `adb uninstall {apkPackageName}`  
5.上传文件 `adb push {localFilePath} {mobileFilePath}`  
6.下载文件 `adb pull {mobileFilePath} {localFilePath}`  
7.点亮/熄灭手机屏幕 `adb shell input keyevent 26`  
8.列出所有的应用的包名 `adb shell pm list package`  

#### 查看手机内部文件
```
# 连接模拟器
adb shell

# 进入/data/data/目录
cd /data/data

# 使用ls命令列出当前目录下所有的包名
pm list package

# 进入指定的包名目录下
cd com.lcj.weather
# 如果出现Permission denied，则输入 su root

# 列出当前目录下的文件及文件夹
ls

# 进入shared_prefs
cd shared_prefs/

# 使用cat/more命令查看文件内容
more com.lcj.weather_preferences.xml
```

#### 查看sqlite数据库信息
```
#以com.lcj.weather为例
adb shell
cd /data/data
cd com.lcj.weather
cd databases/

# 进入sqlite操作界面
sqlite3 cool_weather.db

# 查看所有的表
.tables

# 查看表所有的数据
select * from province;

# 退出sqlite命令行模式
.exit
```
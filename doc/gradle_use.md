### Android Gradle常用命令
相关资料
> https://www.jianshu.com/p/a03f4f6ae31d  
> https://dongchuan.gitbooks.io/gradle-user-guide-/

#### gradlew介绍
- gradlew -> gradle wrapper
- /gradle/wrapper/gralde-wrapper.properties 声明了指向的目录和版本
- grdlew wrapper命令代替全局的gradle命令

#### gradlew常用命令
##### 快速构建命令
```
# 查看构建版本
./gradlew -v
# 清除build文件夹
./gradlew clean
# 检查依赖并编译打包
./gradlew build
# 编译并打印日志
./gradlew build --info
# 调试模式构建并打印日志
./gradlew build --info --debug --stacktrace
# 强制更新最新依赖，清除构建并构建
./gradlew clean --refresh-dependencies build
```
##### 构建并安装debug包
```
# 生成debug包
./gradlew assembleDebug

# 安装debug包
./gradlew installDebug

# 卸载debug包
./gradlew uninstallDebug
```
##### 构建并安装release包
```
# 生成release包
./gradlew assembleRelease

# 安装debug包
./gradlew installRelease

# 卸载debug包
./gradlew uninstallRelease
```
##### 查看包依赖
```
./gradlew dependencies
# 编译时的依赖库
./gradlew app:dependencies --configuration compile
# 运行时的依赖库
./gradlew app:dependencies --configuration runtime
```
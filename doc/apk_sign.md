### Android APK签名
#### 签名的原因
防止发布的应用程序被第三方恶意篡改
- Android采用的是开放的生态系统，任何人都可以开发和发布应用程序给别人使用
- 对于有一些不怀好意的人，完全可以拿到一个原生的应用，然后加入一些恶意的代码，再发布出去，诱使别人去安装，达到不可告人的目的
- Android并不要求所有应用程序的签名证书都由可信任CA的根证书签名，通过这点保证了其生态系统的开放性，所有人都可以用自己生成的证书对应用程序签名

#### 签名步骤
> https://blog.csdn.net/wedfrend/article/details/58044935  
> https://www.cnblogs.com/strinkbug/p/6858804.html  

#### 签名流程分析
1. 逐一遍历里面的所有文件，用SHA1算法提取文件的摘要并进行BASE64编码后，写入MANIFEST.MF文件
2. 计算MANIFEST.MF文件的整体SHA1值，再经过BASE64编码后，记录在CERT.SF主属性块的"SHA1-Digest-Manifest"属性值下；逐条计算MANIFEST.MF文件中每一个块的SHA1，并经过BASE64编码后，记录在CERT.SF中的同名块中
3. 用私钥计算出CERT.SF文件的签名，然后将签名以及包含公钥信息的数字证书一同写入CERT.RSA文件

#### META-INF文件分析
- MANIFEST.MF中记录的是apk中所有文件的摘要值
- CERT.SF中记录的是对MANIFEST.MF的摘要值，包括整个文件的摘要，还有文件中每一项的摘要
- CERT.RSA中记录的是对CERT.SF文件的签名，以及签名的公钥

#### APK安装校验过程
1. 使用CERT.RSA文件中保存的对CERT.SF文件的签名信息和公钥信息来对CERT.SF文件进行验证，判断是否篡改过 
2. 通过CERT.SF文件中记录的摘要值，验证了MANIFEST.MF没有被修改过
3. APK内文件的摘要值要与MANIFEST.MF文件中记录的一致

#### 相关资料
> https://blog.csdn.net/roland_sun/article/details/41825791  
> https://blog.csdn.net/jiangwei0910410003/article/details/50402000  
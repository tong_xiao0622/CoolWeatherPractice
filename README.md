### 练手项目
借鉴的项目
> https://github.com/guolindev/coolweather

#### 相关知识
- [ADB使用介绍](doc/adb_desc.md)
- [Android Gradle常用命令](doc/gradle_use.md)
- [Android APK签名](doc/apk_sign.md)
